import json
import os
import sys
from bottle import route, run, request, response, hook
from gdal_interfaces import GDALTileInterface


DATA_DIR = os.getenv('DATA_DIR', 'data/')
DATA_SUMMARY = os.getenv('DATA_SUMMARY', DATA_DIR + '/summary.json')
HOST = os.getenv('HOST', '0.0.0.0')
PORT = os.getenv('PORT', 10000)
URL_ENDPOINT = os.getenv('URL_ENDPOINT', '/')
WORKERS = os.getenv('WORKERS', cpu_count() * 3)


class InternalException(ValueError):
    """
    Utility exception class to handle errors internally and return error codes to the client
    """
    pass


"""
Initialize a global interface. This can grow quite large, because it has a cache.
"""
interface = GDALTileInterface(DATA_DIR, DATA_SUMMARY)
interface.create_summary_json()


def cpu_count():
    '''
    Returns the number of CPUs in the system
    '''
    if sys.platform == 'win32':
        try:
            num = int(os.environ['NUMBER_OF_PROCESSORS'])
        except (ValueError, KeyError):
            num = 0
    elif 'bsd' in sys.platform or sys.platform == 'darwin':
        comm = '/sbin/sysctl -n hw.ncpu'
        if sys.platform == 'darwin':
            comm = '/usr' + comm
        try:
            with os.popen(comm) as p:
                num = int(p.read())
        except ValueError:
            num = 0
    else:
        try:
            num = os.sysconf('SC_NPROCESSORS_ONLN')
        except (ValueError, OSError, AttributeError):
           num = 0

    if num >= 1:
        return num
    else:
        return 1


def get_elevation(lat, lng):
    """
    Get the elevation at point (lat,lng) using the currently opened interface
    :param lat:
    :param lng:
    :return:
    """
    try:
        elevation = interface.lookup(lat, lng)
    except:
        raise InternalException('No such coordinate (%s, %s)' % (lat, lng))

    return {
        'elevation': elevation,
        'location': {
            'lat': lat,
            'lng': lng
        }
    }


@hook('after_request')
def enable_cors():
    """
    Enable CORS support.
    :return:
    """
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'GET, OPTIONS'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'


def lat_lng_from_location(location_with_comma):
    """
    Parse the latitude and longitude of a location in the format "xx.xxx,yy.yyy" (which we accept as a query string)
    :param location_with_comma:
    :return:
    """
    try:
        lat, lng = [float(i) for i in location_with_comma.split(',')]
        return lat, lng
    except:
        raise InternalException('Bad parameter format "%s".' % location_with_comma)


def query_to_locations():
    """
    Grab a list of locations from the query and turn them into [(lat,lng),(lat,lng),...]
    :return:
    """
    locations = request.query.locations
    if not locations:
        raise InternalException('`Locations` is required.')

    return [lat_lng_from_location(l) for l in locations.split('|')]


def do_lookup(get_locations_func):
    """
    Generic method which gets the locations in [(lat,lng),(lat,lng),...] format by calling get_locations_func
    and returns an answer ready to go to the client.
    :return:
    """
    try:
        locations = get_locations_func()
        return {
            'results': [get_elevation(lat, lng) for (lat, lng) in locations],
            'success': 'true'
        }
    except InternalException as e:
        response.status = 400
        response.content_type = 'application/json'
        return {
            'results': [],
            'success': 'false',
            'message': e.args[0]
        }


# For CORS
@route(URL_ENDPOINT, method=['OPTIONS'])
def cors_handler():
    return {}

@route(URL_ENDPOINT, method=['GET'])
def get_lookup():
    """
    GET method. Uses query_to_locations.
    :return:
    """
    return do_lookup(query_to_locations)


run(host=HOST, port=PORT, server='gunicorn', workers=WORKERS)
