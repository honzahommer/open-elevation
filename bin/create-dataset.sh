#!/usr/bin/env bash

: ${DATA_DIR="data"}

_DIRNAME="$(dirname "$PWD/$0")"
OUTDIR="$_DIRNAME/../$DATA_DIR"

export OUTDIR
mkdir -p "$OUTDIR"
cd "$OUTDIR"

echo Downloading data...
"$_DIRNAME/download-srtm-data.sh"

echo create tiles: SRTM_NE_250m
"$_DIRNAME/create-tiles.sh" SRTM_NE_250m.tif 10 10
rm -rf SRTM_NE_250m.tif

echo create tiles: SRTM_SE_250m
"$_DIRNAME/create-tiles.sh" SRTM_NE_250m.tif 10 10
rm -rf SRTM_SE_250m.tif

echo create tiles: SRTM_W_250m
"$_DIRNAME/create-tiles.sh" SRTM_NE_250m.tif 10 20
rm -rf SRTM_W_250m.tif
