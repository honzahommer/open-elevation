#!/usr/bin/env bash

: ${APP_USER="app"}
: ${APP_DIR="/home/$APP_USER/open-elevation"}

export DEBIAN_FRONTEND=noninteractive
export CPLUS_INCLUDE_PATH=/usr/include/gdal
export C_INCLUDE_PATH=/usr/include/gdal

echo 'Acquire::ForceIPv4 "true";' > /etc/apt/apt.conf.d/99force-ipv4

apt-get -yq update
apt-get -yq upgrade
apt-get -yq autoremove
apt-get -yq install bc git gdal-bin python-gdal libgdal-dev python3-rtree python-pip unar wget

id "$APP_USER" > /dev/null 2>&1 \
  || adduser --disabled-password --gecos "" "$APP_USER"

[ ! -d "$APP_DIR" ] \
  && mkdir -p "$APP_DIR" && chown "$APP_USER" "$APP_DIR"

su "$APP_USER" -c "git clone --depth=1 https://gitlab.com/honzahommer/open-elevation.git '$APP_DIR'"
su "$APP_USER" -c "pip install -r '$APP_DIR/requirements.txt'"
su "$APP_USER" -c "'$APP_DIR/bin/create-dataset.sh'"

cat <<EOT > /etc/systemd/system/open-elevation.service
[Unit]
Description=Open-Elevation Server
After=network.target

[Service]
Type=simple
User=$APP_USER
WorkingDirectory=$APP_DIR
ExecStart=/usr/bin/env python server.py
Restart=always
RestartSec=1

[Install]
WantedBy=multi-user.target
EOT

systemctl enable open-elevation.service
systemctl start open-elevation.service
